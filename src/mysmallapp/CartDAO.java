package mysmallapp;

import org.hibernate.SessionFactory;
import org.hibernate.*;
import java.util.*;

public class CartDAO {

    SessionFactory factory = null;
    Session session = null;

    private static CartDAO single_instance = null;

    private CartDAO() { factory = HibernateUtils.getSessionFactory();}

    public static CartDAO getInstance() {
        if (single_instance == null) {
            single_instance = new CartDAO();
        }
        return single_instance;
    }


    public void saveCart() {

        Cart cart = new Cart();
        cart.setName("My Cart");

        Items item1 = new Items("ID123", 10.00, 5, cart);
        Items item2 = new Items("ID234", 50.00, 10, cart);
        Set<Items> itemsSet = new HashSet<Items>();
        itemsSet.add(item1); itemsSet.add(item2);

        Transaction tx = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            session.save(cart);
            session.save(item1);
            session.save(item2);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        } finally {
            if(!factory.isClosed()) {
                factory.close();
            }
        }
    }


}
