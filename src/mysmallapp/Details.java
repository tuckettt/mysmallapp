package mysmallapp;

import java.util.*;
import javax.persistence.*;

@Entity
@Table(name="details")
public class Details {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "details_id")
    private long detailsID;

    @Column(name = "recipename")
    private String name;


    @OneToMany(mappedBy = "detailsid")
    private Set<Ingredients> ingredients;

    public Details() {}

    public String toString() {
        String results = getName();
/*
        for(Ingredients nam : ingredients) {
            results = results + " " + nam.getIngredient();
        }
*/
        return results;
    }

    public long getDetailsID() {
        return detailsID;
    }

    public void setDetailsID(long detailsID) {
        this.detailsID = detailsID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Ingredients> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<Ingredients> ingredients) {
        this.ingredients = ingredients;
    }
}
