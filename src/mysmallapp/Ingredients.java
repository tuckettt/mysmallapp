package mysmallapp;

import javax.persistence.*;

@Entity
@Table(name="ingredients")
public class Ingredients {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int ingredientID;

    @Column(name = "ingredient")
    private String ingredient;

    @ManyToOne
    @JoinColumn(name = "details_id", nullable = false)
    private Details detailsid;

    public Ingredients() {}
    public Ingredients(String ingredient, Details detailsid) {
        this.setIngredient(ingredient); this.setDetailsid(detailsid);
    }

    public String toString() {
        return getIngredient();
    }


    public int getIngredientID() {
        return ingredientID;
    }

    public void setIngredientID(int ingredientID) {
        this.ingredientID = ingredientID;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public Details getDetailsid() {
        return detailsid;
    }

    public void setDetailsid(Details detailsid) {
        this.detailsid = detailsid;
    }
}
