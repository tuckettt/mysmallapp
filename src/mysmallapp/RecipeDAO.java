package mysmallapp;

import org.hibernate.*;
import java.util.*;

public class RecipeDAO {

    SessionFactory factory = null;
    Session session = null;

    private static RecipeDAO single_instance = null;

    private RecipeDAO() {factory = HibernateUtils.getSessionFactory();}

    public static RecipeDAO getInstance() {
        if (single_instance == null) {
            single_instance = new RecipeDAO();
        }
        return single_instance;
    }

    public void saveDetails() {

            Details dets = new Details();
            dets.setName("caramel");

            Ingredients ing1 = new Ingredients("butter", dets);
            Ingredients ing2 = new Ingredients("sugar", dets);

            Set<Ingredients> recipeSet = new HashSet<Ingredients>();
            recipeSet.add(ing1); recipeSet.add(ing2);

            Transaction tx = null;

        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            session.save(dets);
            session.save(ing1);
            session.save(ing2);

            tx.commit();

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {

        }
    }

    public void addRecipe(Details d, Set<Ingredients> recipeSet) {

        Transaction tx = null;

        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            session.save(d);

            for(Ingredients nam : recipeSet) {
                session.save(nam);
            }

            tx.commit();

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {

        }
    }

    public Details getDetails(int id) {

        Transaction tx = null;
        try {
            session = factory.openSession();
            tx = session.beginTransaction();
            String sql = "from mysmallapp.Details where detailsID=" + Integer.toString(id);
            //Customer c = (Customer)session.createQuery(sql).getSingleResult();
            Details d = (Details)session.createQuery(sql).uniqueResult();
            tx.commit();
            return d;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
//
        }
    }
}