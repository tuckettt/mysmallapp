package mysmallapp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashSet;
import java.util.Set;

public class JsonMapping {

    ObjectMapper objectMapper = null;
    Details dets = null;

    public JsonMapping() {
        objectMapper = new ObjectMapper();
        dets = new Details();
    }

    public static String toJson(Object object) {
        ObjectMapper map = new ObjectMapper();
        try {
            return map.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Object toClass(String json) {
        ObjectMapper map = new ObjectMapper();
        try {
            return map.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String mapDetails() {

        dets.setName("Chili");

        try {
            return objectMapper.writeValueAsString(dets);
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
            return null;
        }
    }

    public String mapIngredients() {

        Ingredients ing1 = new Ingredients("beans", dets);
        Ingredients ing2 = new Ingredients("meat", dets);

        Set<Ingredients> recipeSet = new HashSet<Ingredients>();
        recipeSet.add(ing1); recipeSet.add(ing2);

        try {
            return objectMapper.writeValueAsString(recipeSet);
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
            return null;
        }
    }
}