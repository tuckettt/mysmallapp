import mysmallapp.Details;
import mysmallapp.Ingredients;
import mysmallapp.RecipeDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

@WebServlet(name = "AddRecipe", urlPatterns={"/AddRecipe"})
public class AddRecipe extends HttpServlet {

    RecipeDAO r = RecipeDAO.getInstance();
    //CartDAO c = CartDAO.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();



        Details dets = new Details();
        dets.setName(request.getParameter("name"));

        Ingredients ing1 = new Ingredients(request.getParameter("ing1"), dets);
        Ingredients ing2 = new Ingredients(request.getParameter("ing2"), dets);

        Set<Ingredients> recipeSet = new HashSet<Ingredients>();
        recipeSet.add(ing1); recipeSet.add(ing2);

        r.addRecipe(dets, recipeSet);
        response.setContentType("text/html");
        out.println("<html><head></head><body>" + "Record Saved" + "</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
        //out.println(r.getDetails(1));
    }
}
