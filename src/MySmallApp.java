import mysmallapp.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MySmallApp", urlPatterns={"/MySmallApp"})
public class MySmallApp extends HttpServlet {

    RecipeDAO r = RecipeDAO.getInstance();
    //CartDAO c = CartDAO.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        //r.saveDetails();
        //c.saveCart();
        Details dets = null;
        dets = r.getDetails(23);
        response.setContentType("text/html");
        out.println("<html><head></head><body>" + dets.toString() + "</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
        //out.println(r.getDetails(1));
    }
}
