<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
</head>
<body>
<h1>Add a Recipe</h1>
<form action="AddRecipe" method="post">
    <Table>
        <tr>
            <td>Recipe Name</td>
            <td><input type="text" name="name"/></td>
        </tr>
        <tr>
            <td>Ingredient 1</td>
            <td> <input type="text" name="ing1"/> </td>
        </tr>
        <tr>
            <td> Ingredient 2</td>
            <td> <input type="text" name="ing2"/></td>
        </tr>
        <tr>
            <td colspan="2"> <input type="submit" value="Save"/></td>
        </tr>
    </Table>

</form>
</body>
</html>

